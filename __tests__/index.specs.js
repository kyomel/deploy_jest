const request = require('supertest');
const app = require('../index');

describe('Root Endpoint', () => {
  describe('GET /', () => {
    test('Should return 200', done => {
      request(app).get('/')
        .then(res => {
          expect(res.body.status).toEqual('success');
          expect(res.body.message).toEqual('Bukak sithik Jest!');
          done();
        })
    })
  })
})
