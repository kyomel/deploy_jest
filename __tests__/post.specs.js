const request = require('supertest');
const app = require('../index');
const { Post } = require('../models');

describe('Post API Collection', () => {

  beforeAll(() => {
    return Post.destroy({
      truncate: true
    })
  })

  afterAll(() => {
    return Post.destroy({
      truncate: true
    })
  })

  describe('POST /api/v1/posts', () => {
    test('Should succesfully create a new post', done => {
      request(app).post('/api/v1/posts')
        .set('Content-Type', 'application/json')
        .send({ title: 'Hello World', body: 'Lorem Ipsum' })
        .then(res => {
          expect(res.statusCode).toEqual(201);
          expect(res.body.status).toEqual('success');
          expect(res.body.data).toHaveProperty('post');
          done();
        })
    })
  })
})
