const router = require('express').Router();

// Controller
const index = require('./controllers/indexController.js');
const post = require('./controllers/postController.js');

// Root Endpoint
router.get('/', index.index);

// Post API Collection
router.get('/api/v1/posts', post.index);
router.post('/api/v1/posts', post.create);

module.exports = router;
