const { Post } = require('../models');

module.exports = {

  // GET /api/v1/posts
  index(req, res) {
    // Find all post
    Post.findAll()
      .then(posts => {

        // Serve response
        res.status(200).json({
          status: 'success',
          data: {
            posts
          }
        })

      })
  },

  create(req, res) {
    Post.create(req.body)
      .then(post => {
        res.status(201).json({
          status: 'success',
          data: {
            post
          }
        })
      })
      .catch(err => {
        res.status(400).json({
          status: 'fail',
          message: [err.message]
        })
      })
  }
}
