// Keep the import above
const express = require('express');
const morgan = require('morgan');

const dotenv = require('dotenv')
dotenv.config();

const router = require('./router');

const app = express();

if (process.env.NODE_ENV !== 'test')
  app.use(morgan('dev'));
app.use(express.json());
app.use(router);

module.exports = app;
